import 'package:flutter/material.dart';

void main() {
  runApp(SecondApp());
}

class SecondApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double iconSize = 40.0;
    final TextStyle textStyle = new TextStyle(
        color: Colors.grey, fontSize: 20.0
    );

    return MaterialApp(
      title: "Prueba 2 de app",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text("Second App"),
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new MyCard(title: new Text("This is a message", style: textStyle,),
                  icon: new Icon(Icons.favorite, size: iconSize, color: Colors.red,)),
              new MyCard(title: new Text("I like it", style: textStyle,),
                  icon: new Icon(Icons.search, size: iconSize, color: Colors.blue,)),
              new MyCard(title: new Text("Next video", style: textStyle,),
                  icon: new Icon(Icons.queue_play_next, size: iconSize, color: Colors.yellow,))
            ],
          ),
        )
      ),
    );
  }
}

class MyCard extends StatelessWidget {
  final Widget title;
  final Widget icon;

  MyCard({this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: new Card(
        child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[this.title, this.icon],
            )
        ),
      ),
    );
  }
}

class PrincipalApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Fluuter Demo App',
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: null,
          ),
          title: Text('Flutter Demo'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: null,
            ),
          ],
        ),
        body: Container(
            color: Colors.grey[300],
            padding: EdgeInsets.symmetric(vertical: 15.0),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Text("Row 1"),
                    Text("Row 2"),
                    Text("Row 3"),
                    Image.asset("images/hollow_knight.png", height: 50, width: 50,),
                    Icon(
                        Icons.favorite,
                        color: Colors.pink,
                        size: 24
                    )
                  ],
                ),
                Text("###########"),
                Row(
                  children: [
                    Icon(
                        Icons.access_alarm,
                        color: Colors.red,
                        size: 36
                    ),
                    Icon(
                        Icons.airline_seat_recline_extra,
                        color: Colors.green,
                        size: 24
                    )
                  ],
                )
              ],
            )
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: null,
        ),
      ),
    );
  }
}
